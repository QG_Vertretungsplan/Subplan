<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Fetch and store data.
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Return plan data. Fetch it from website if necessary or desired and save it in db.
 * @return \block_subplan\output\substitution_plan_renderable|null renderable
 */
function get_plan_renderable() {
    global $DB;

    $dbtable = "subplan";
    $age = time() - $DB->get_field_select($dbtable, 'updated', 'id = :id', array('id' => 1));

    // Fetch plan from website if the db is empty or if the data there is older than the max age specified in the settings.
    if (
        $DB->count_records($dbtable) == 0 ||
        $age > get_config("block_subplan", "update_interval")
    ) {
        save_plan_in_db(make_renderable(fetch_from_url()));
    }

    // Is there plan data in the db?
    if ($DB->count_records($dbtable) != 0) {
        // Fetch plan data from db.
        $plandata = unserialize($DB->get_field_select($dbtable, 'plan', 'id = :id', array('id' => 1)));
        // Return empty array if db is empty.
    } else {
        $plandata = new \block_subplan\output\substitution_plan_renderable(null);
    }
    return $plandata;
}

/**
 * Fetch the plan data from the website specified in the settings using curl
 * @return string|null NULL if there was an error, otherwise the content of the website
 */
function fetch_from_url() {
    $ch = curl_init(get_config("block_subplan", "url"));
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_exec($ch);
    if (curl_error($ch)) {
        return null;
    }
    $out = curl_multi_getcontent($ch);
    curl_close($ch);
    return $out;
}

/**
 * Turn input (fetched from URL) into renderable \block_subplan\output\substitution_plan_renderable
 *
 * @param string|null $input from URL
 * @return \block_subplan\output\substitution_plan_renderable|null
 */
function make_renderable($input) {
    if (is_null($input)) {
        return null;
    }

    $parser = null;
    // Add parsers here.
    switch (get_config('block_subplan', 'selected_parser')) {
        case "0":
            $parser = new \block_subplan\parsers\qg_timesub_parser($input);
            break;
        default:
            return null;
    }

    $plandataarray = $parser->get_days($input);
    if (is_null($plandataarray)) {
        return null;
    }
    return new \block_subplan\output\substitution_plan_renderable($plandataarray);
}

/**
 * Save plan data array in db if the data is valid
 * @param \block_subplan\output\substitution_plan_renderable|null $substitutionplan plan data
 */
function save_plan_in_db($substitutionplan) {
    global $DB;

    $dbtable = "subplan";
    if (!is_null($substitutionplan)) {
        $serialized = serialize($substitutionplan);
        $dbentry = new stdClass();
        $dbentry->plan = $serialized;
        $dbentry->id = 1;
        $dbentry->updated = time();
        if ($DB->count_records($dbtable) == 0) {
            $DB->insert_record($dbtable, $dbentry);
        } else {
            $DB->update_record($dbtable, $dbentry);
        }
    }
}
