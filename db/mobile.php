<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines mobile handlers.
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

$addons = [
    'block_subplan' => [
        'handlers' => [
            'subplan' => [
                'delegate' => 'CoreBlockDelegate',
                'method' => 'view_mobile',
                'displaydata' => [
                    'title' => 'pluginname',
                    'class' => 'block_subplan'
                ],
                'styles' => [
                    'url' => $CFG->wwwroot . '/blocks/subplan/mobile.css',
                    'version' => 2022052303
                ],
            ],
        ],
        'lang' => [
            ['pluginname', 'block_subplan'],
            ['substitutions', 'block_subplan'],
            ['please_regard', 'block_subplan'],
            ['subject', 'block_subplan'],
            ['no_plan', 'block_subplan'],
            ['please_contact', 'block_subplan'],
            ['attention', 'block_subplan'],
            ['old_plan', 'block_subplan'],
            ['nosubstitutions', 'block_subplan'],
            ['absentcourses', 'block_subplan'],
            ['absentclasses', 'block_subplan'],
            ['absentteachers', 'block_subplan'],
            ['missingrooms', 'block_subplan']
        ]
    ],
];
