# Subplan Moodle Plugin

Mit diesem Moodle-Plugin können Vertretungspläne als Blöcke angezeigt werden.
Dieses Plugin ist auf unsere Schule und die dort verwendeten Programme (Time 2007, OpenSchulportfolio) zugeschnitten. Wenn sie einen Parser zum Plugin hinzufügen, können aber auch Vertretungspläne von anderen Programmen angezeigt werden.

## Überblick

Dieses Moodle-Plugin stellt einen Block bereit, der auf allen Seiten im Moodle (auch auf der MyMoodle-Seite) angezeigt werden kann. Außerdem bietet das Plugin Unterstützung für die Moodle-Mobile-App.

Die Vertretungsplan-Daten müssen über einen Webserver abrufbar sein (derzeit werden nur GET-Requests unterstützt).

Der Vertretungsplan kann beliebig viele Tage unterstützen. Jeder Tag besteht aus bis zu drei Teilen: Zuerst wird eine "Bitte Beachten"-Nachricht in einem roten Kasten angezeigt. Danach können weitere allgemeine Informationen angezeigt werden, die aus einem kurzen Namen (wird fett gedruckt) und einer Nachricht bestehen. Anschließend kommt die Tabelle, in der alle Vertretungen aufgelistet sind. Die Tabelle kann beliebig viele Zeilen und Spalten enthalten.

Es können für Schüler und Lehrer unterschiedliche Vertretungspläne erzeugt werden (z.B. weil Schüler den Vertretungsplan nach Klasse sortiert haben wollen, Lehrer aber nach Lehrernamen). Benutzer können zwischen der Schüler- und der Lehrerversion wechseln. Die ausgewählte Version wird gespeichert und beim nächsten Mal direkt angezeigt.

## Parser
Bislang gibt es nur einen Parser, um Vertretungspläne aus `Timesub 2007`, die mit dem `OpenSchulPortfolio` erzeugt wurden, anzuzeigen.

Weitere Parser können in `classes/parsers` hinzugefügt werden. Sie sollten von der Klasse `classes/parsers/parser_base.php` erben. Diese Klasse stellt auch sicher, dass der Input korrekt bereinigt wird, damit keine XSS-Attacken möglich sind. Aufgrund der Input-Bereinigung wird HTML derzeit nur im `Bitte Beachten` unterstützt (um Zeilenumbrüche zu ermöglichen). HTML in der Tabelle kann durch Replacements hinzugefügt werden (siehe `Einstellungen`) Außerdem gibt es eine Methode, mit der die oben erwähnten zusätlichen Informationen erzeugt werden können. Der Parser muss dann noch in `settings.php` (damit man ihn in den Einstellungen auswählen kann) und in `data.php -> make_renderable()` (hier wird der gewünschte Parser aufgerufen) hinzugefügt werden.

## Einstellungen
Unter `>Website-Administration>Plugins>Blöcke>Vertretungsplan` befinden sich die Einstellungen.
- URL: Die URL, unter der die Daten für den Vertretungsplan abrufbar sind.
- Update Intervall: Um den Server zu schonen, sollten die Daten für den Vertretungsplan nicht bei jedem Aufruf neu geladen werden. In dieser Einstellung kann angegeben werden, wie lange der letzte Aufruf her sein muss, damit die Daten neu abgerufen werden.
- Planwarnungszeitpunkt: Wenn der Server, von dem die Daten für den Vertretungsplan abgerufen werden, nicht erreicht werden kann, werden die Daten angezeigt, die beim letzten Aufruf geladen wurden. Nach der hier angegebenen Zeit wird dann eine Warnung angezeigt.
- Mail eines Admins: *Optional* Wenn kein Vertretungsplan geladen werden konnte, oder wenn die letzte erfolgreiche Aktualisierung der Daten schon länger her ist, als in *Planwarnungszeitpunkt* angegeben, werden Warnungen angezeigt. Diese E-Mail-Adresse wird in den Warnungen angezeigt, damit die Nutzer einen Administrator benachrichtigen können.
- Spalten für Replacements Im Schülerplan: Die Nummern der Spalten, in denen Replacements angewendet werden sollen (beginnend bei 1).
- Spalten für Replacements im Lehrerplan: Die Nummern der Spalten, in denen Replacements angewendet werden sollen (beginnend bei 1).
- Replacements: Hier können Replacement-Regeln angegeben werden. In den jeweiligen Spalten wird dann jedes Vorkommen von *needle* durch *replacement* ersetzt. Außerdem erhält der Text *replacement* die CSS-Klasse *class*. Es können mehrere replacements angegeben werden. Dabei muss folgendes Format eingehalten werden:
```
1 needle | replacement | class ;
2 needle | replacement | class ;
3 ...
```
Beispiel: Siehe Standardeinstellung.

Von subplan werden folgende CSS-Klassen hinzugefügt, die auch in den Replacements verwendet werden können:
  - `subplan-green` fügt ein grünes Quadrat vor dem Text ein
  - `subplan-orange` fügt ein oranges Quadrat vor dem Text ein
  - `subplan-red` fügt ein rotes Quadrat vor dem Text ein
  - `subplan-highlight` gibt dem Text einen gelben Hintergrund mit abgerundeten Ecken

## Weitere Informationen

- Um den Vertretungsplan-Block zum Dashboard aller Nutzer hinzuzufügen, gehen sie zu *Website-Administration > Darstellung > Dashboard für alle anpassen*. Siehe auch: https://docs.moodle.org/39/de/Dashboard#Standardm.C3.A4.C3.9Figes_Dashboard_gestalten
- Wenn der Block auf dem Dashboard aller Nutzer angezeigt wird, kann er auch von Gästen eingesehen werden. Er ist sozusagen öffentlich zugänglich. Um dies zu verhindern, können sie die Berechtigungen für den Block anpassen. Gehen sie zu *Website-Administration > Darstellung > Dashboard für alle anpassen*, aktivieren sie die Blockbearbeitung und klicken sie auf das Zahnrad im Vertretungsplan-Block. Wählen sie dort *Rechte ändern* aus. Klicken sie neben *Block sehen* auf den Mülleimer neben *Gast*.