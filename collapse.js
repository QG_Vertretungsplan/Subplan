var buttons = document.getElementsByClassName("subplan-collapse-button");
for (let i = 0; i < buttons.length; i++) {
  buttons[i].addEventListener("click", function() {
    // set class for rotating the icon
    this.classList.toggle("subplan-collapse-button-active");

    // collapse or expand the content and update aria-expanded for accessibility
    var content = document.getElementById(this.getAttribute("data-block-subplan-target"));
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
      this.setAttribute("aria-expanded", "false");
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
      this.setAttribute("aria-expanded", "true");
    }
  });}