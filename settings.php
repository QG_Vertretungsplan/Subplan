<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cross-block-instance settings
 *
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$settings->add(new admin_setting_configtext(
    'block_subplan/url',
    get_string('urlconfig', 'block_subplan'),
    get_string('urlconfigdesc', 'block_subplan'),
    '',
    PARAM_URL
));

$settings->add(new admin_setting_configselect(
    'block_subplan/selected_parser',
    get_string('selected_parser', 'block_subplan'),
    get_string('selected_parser_desc', 'block_subplan'),
    0,
    array(0 => 'QG Timesub')
));

$settings->add(new admin_setting_configcheckbox(
    'block_subplan/enableteacherversion',
    get_string('enableteacherversion', 'block_subplan'),
    get_string('enableteacherversiondesc', 'block_subplan'),
    '0'
));

$settings->add(new admin_setting_configduration(
    'block_subplan/update_interval',
    get_string('update_interval', 'block_subplan'),
    get_string('update_interval_desc', 'block_subplan'),
    300,
    60
));

$settings->add(new admin_setting_configduration(
    'block_subplan/tooOld',
    get_string('tooOld', 'block_subplan'),
    get_string('tooOlddesc', 'block_subplan'),
    14400,
    60 * 60
));

$settings->add(new admin_setting_configtext(
    'block_subplan/adminmail',
    get_string('adminmail', 'block_subplan'),
    get_string('adminmaildesc', 'block_subplan'),
    '',
    PARAM_EMAIL
));

$settings->add(new admin_setting_configtext(
    'block_subplan/toreplacestudents',
    get_string('toreplacestudents', 'block_subplan'),
    get_string('toreplacedesc', 'block_subplan'),
    '4,5,7',
    PARAM_SEQUENCE
));

$settings->add(new admin_setting_configtext(
    'block_subplan/toreplaceteachers',
    get_string('toreplaceteachers', 'block_subplan'),
    get_string('toreplacedesc', 'block_subplan'),
    '7',
    PARAM_SEQUENCE
));

$settings->add(new admin_setting_configtextarea(
    'block_subplan/replacements',
    get_string('replacements', 'block_subplan'),
    get_string('replacementsdesc', 'block_subplan'),
    '1 entfällt     |(f.a.) | subplan-green ;
    2 Raumänderung |(RÄ)   | subplan-orange ;
    3 Vertretung   |Vertr. | subplan-red ;
    4 Zusatzstunde |Zusatzst. | subplan-red ;
    5 Extrastunde |Zusatzst. | subplan-red ;
    6 ** |Neu | subplan-highlight;',
    PARAM_TEXT
));
