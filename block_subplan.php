<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Substitution plan block main class.
 *
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once('data.php');

/**
 * The subplan block class
 */
class block_subplan extends block_base {

    /**
     * Function called by moodle to initialize the plugin. Set title to display.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_subplan');
    }

    /**
     * Function called by moodle to enable config file
     */
    public function has_config() {
        return true;
    }

    /**
     * Function called by moodle to get content for the block
     * @return stdClass content of the block
     */
    public function get_content() {
        global $CFG;
        $renderable = get_plan_renderable();
        $renderable->set_mobile_version(false);
        $output = $this->page->get_renderer('block_subplan');
        $this->content = new stdClass;

        // Update user preference regarding student/teacher version if necessary and permitted.
        if (get_config("block_subplan", "enableteacherversion")) {
            $teacherpref = (bool) get_user_preferences("block_subplan_teacher", false);
            $teacherselected = (bool) optional_param("block_subplan_teacher", $teacherpref, PARAM_BOOL);
            if ($teacherpref !== $teacherselected) {
                set_user_preference("block_subplan_teacher", $teacherselected);
            }
        } else {
            set_user_preference("block_subplan_teacher", false);
        }

        $this->content->text = $output->render($renderable);

        // Include js for collapsible.
        $this->page->requires->js(new moodle_url($CFG->wwwroot . '/blocks/subplan/collapse.js'));

        return $this->content;
    }
}
