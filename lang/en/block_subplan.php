<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language strings.
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['pluginname'] = 'Substitution Plan';
$string['subplan'] = 'Subplan';
$string['subplan:addinstance'] = 'Add a new Subplan block.';
$string['subplan:myaddinstance'] = 'Add a new Subplan block to the My Moodle page.';
$string['urlconfig'] = 'URL';
$string['urlconfigdesc'] = 'Where to get the substitution plan data from.';
$string['update_interval'] = 'Update Interval';
$string['update_interval_desc'] = 'Update Interval for the Caching of Subplan in minutes.';
$string['replacements'] = 'Replacements';
$string['replacementsdesc'] = 'Replacements for the Subplan.  Format: Number ToReplace | ReplaceWith | CSS-Class ;';
$string['toreplacestudents'] = 'Columns for replacements in the student plan';
$string['toreplaceteachers'] = 'Columns for replacements in the teacher plan';
$string['toreplacedesc'] = 'Numbers of the columns, where the replacements should be applied. Columns are numbered from left to right, starting at 1.';
$string['tooOld'] = 'Plan too old after';
$string['tooOlddesc'] = 'Time after that a warning is displayed when the plan is from database.';
$string['adminmail'] = 'Mail of an Admin';
$string['adminmaildesc'] = 'Optional: E-Mail adress of an administrator. Shown by errors.';
$string['substitutions'] = 'Substitutions';
$string['please_regard'] = "Please regard";
$string['subject'] = "Subject";
$string['no_plan'] = "Unfortunately, there is no substitution plan available at the moment.";
$string['please_contact'] = "Please contact an administrator, if the problem has existed for some time! E-mail:";
$string['old_plan'] = "The substitution plan is not up to date. Time of the now visible plan:";
$string['attention'] = "Attention!";
$string['nosubstitutions'] = "There are currently no substitutions.";
$string['selected_parser'] = "Format";
$string['selected_parser_desc'] = "The format of the data that can be retrieved from the URL.";
$string['absentcourses'] = "Absent courses";
$string['absentclasses'] = "Absent classes";
$string['absentteachers'] = "Absent teachers";
$string['missingrooms'] = "Missing rooms";
$string['toteacherversion'] = "To teacher version";
$string['tostudentversion'] = "To student version";
$string['enableteacherversion'] = "Enable teacher version";
$string['enableteacherversiondesc'] = "Allows users to choose between the student and the teacher version (regardless of whether they actually are a teacher or not). Otherwise, everyone will get to see the student version.";
