<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language strings.
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['pluginname'] = 'Vertretungsplan';
$string['subplan'] = 'Subplan';
$string['subplan:addinstance'] = 'Füge einen neuen Subplan-Block hinzu.';
$string['subplan:myaddinstance'] = 'Füge einen neuen Subplan-Block zu meiner Moodle Seite hinzu.';
$string['urlconfig'] = 'URL';
$string['urlconfigdesc'] = 'Von wo die Vertretungsplandaten geladen werden sollen.';
$string['update_interval'] = 'Update Intervall';
$string['update_interval_desc'] = 'Update Intervall für das Caching des Vplans in Minuten.';
$string['replacements'] = 'Replacements';
$string['replacementsdesc'] = 'Replacements für den Vplan. Format: Nummer ZuErsetzen | ErsetzenDurch | CSS-Klasse ;';
$string['toreplacestudents'] = 'Spalten für Replacements im Schülerplan';
$string['toreplaceteachers'] = 'Spalten für Replacements im Lehrerplan';
$string['toreplacedesc'] = 'Nummern der Spalten, in denen Replacements ausgeführt werden sollen. Die Spalten sind von links nach rechts nummeriert, angefangen bei 1.';
$string['tooOld'] = 'Planwarnungszeitpunkt';
$string['tooOlddesc'] = 'Zeit ab wann eine Warnung kommt, wenn der Plan aus der Datenbank zu alt ist.';
$string['adminmail'] = 'Mail eines Admins';
$string['adminmaildesc'] = 'Optional: E-Mail Adresse eines Administrators, die bei Fehlern angezeigt wird.';
$string['substitutions'] = 'Vertretungen';
$string['please_regard'] = "Bitte beachten";
$string['subject'] = "Fach";
$string['no_plan'] = "Momentan ist leider kein Vertretungsplan verfügbar.";
$string['please_contact'] = "Bitte kontaktieren Sie einen Administrator, falls das Problem bereits seit einiger Zeit besteht! E-Mail:";
$string['old_plan'] = "Der Vertretungsplan ist nicht aktuell. Zeitpunkt des jetzt sichtbaren Plans:";
$string['attention'] = "Achtung!";
$string['nosubstitutions'] = "Zurzeit gibt es keine Vertretungen.";
$string['selected_parser'] = "Format";
$string['selected_parser_desc'] = "Das Format der Daten, die über die URL abgerufen werden können.";
$string['absentcourses'] = "Abwesende Kurse";
$string['absentclasses'] = "Abwesende Klassen";
$string['absentteachers'] = "Abwesende Lehrer";
$string['missingrooms'] = "Fehlende Räume";
$string['toteacherversion'] = "Zur Lehrerversion";
$string['tostudentversion'] = "Zur Schülerversion";
$string['enableteacherversion'] = "Lehrerversion aktivieren";
$string['enableteacherversiondesc'] = "Wenn aktiviert, können alle Benutzer zwischen der Schüler- und der Lehrerversion wählen (unabhängig davon, ob sie tatsächlich ein Lehrer sind). Andernfalls wird allen Benutzern die Schülerversion angezeigt.";
