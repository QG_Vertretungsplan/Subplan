<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class that holds the actual needed data for a single day of a substitution plan.
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_subplan;

/**
 * Class that holds the actual needed data for a single day of a substitution plan.
 */
class day_data {

    /** See __construct for a description of these attributes. */
    public $longdate;
    public $printdate;
    public $version;
    public $pleaseregard;
    public $headings;
    public $rows;
    public $additionalinfo;

    /**
     * Construct a day_data object.
     *
     * @todo Sanitize all inputs.
     *
     * @param string $longdate Date for which the data is valid. Example: "Dienstag, 11.01.2022"
     * @param string $printdate Dateo n which the data was created. Example: "vom 11.01.22 um 9:23"
     * @param string $version revision number. Example: "Version 3"
     * @param string $pleaseregard A general, important message for this day.
     * @param string[] $headings headings for the table columns.
     * @param array $rows array of string[] which contain data for the cells in a row.
     * @param array $additionalinfo array of string[] which contains two entries: first the name, then the content.
     */
    public function __construct($longdate, $printdate, $version, $pleaseregard, $headings, $rows, $additionalinfo) {
        $this->longdate = s($longdate);
        $this->printdate = s($printdate);
        $this->version = s($version);
        // The pleaseregard may contain special characters for line breaks, so we can't use s().
        $this->pleaseregard = format_text($pleaseregard, FORMAT_HTML);
        $this->sanitize_arr($headings);
        $this->headings = $headings;
        for ($i = 0; $i < count($rows); $i++) {
            $this->sanitize_arr($rows[$i]);
        }
        $this->rows = $rows;
        for ($i = 0; $i < count($additionalinfo); $i++) {
            $this->sanitize_arr($additionalinfo[$i]);
        }
        $this->additionalinfo = $additionalinfo;
    }

    /**
     * Sanitizes all fields of an array by applying s() to them. Works on a reference.
     *
     * @param string[] $row
     * @return void
     */
    private function sanitize_arr(&$row) {
        foreach ($row as $key => $value) {
            $row[$key] = s($value);
        }
    }
}
