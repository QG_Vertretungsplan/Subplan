<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Parses raw data in the qg timesub format.
 *
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_subplan\parsers;

/**
 * Parses raw data in the qg timesub format.
 */
class qg_timesub_parser extends parser_base {

    /**
     * Decoded input or null if the input was no valid json.
     *
     * @var array|null
     */
    private $decoded;

    /**
     * Constructs object with decoded json.
     *
     * @param string $input json
     */
    public function __construct($input) {
        $this->decoded = json_decode($input, true);
    }

    /**
     * Get data for all days.
     *
     * @return array|null array of two \block_subplan\output\day_data[], the first is for student, the latter for teachers.
     * Returns null if the data is invalid or empty.
     */
    public function get_days() {
        if (is_null($this->decoded) || empty($this->decoded)) {
            return null;
        }
        $variants = array();
        foreach (array('aula', 'lehrer') as $displaytype) {
            $days = array();
            foreach ($this->decoded as $day) {
                $tstattext = $day['tstattext' . $displaytype];
                $tdyntext = $day['tdyntext' . $displaytype];

                list($longdate, $printdate, $version, $pleaseregard, $headings) = $this->get_tstattext_content($tstattext);

                // Only teachers need to see additional info.
                $additionalinfo = array();
                if ($displaytype === "lehrer") {
                    $additionalinfo = $this->get_additional_info($tstattext);
                }

                $rows = $this->get_rows($tdyntext);

                $daydata = new \block_subplan\day_data($longdate, $printdate, $version, $pleaseregard, $headings, $rows,
                    $additionalinfo);
                array_push($days, $daydata);
            }
            array_push($variants, $days);
        }
        return $variants;
    }

    /**
     * Return the variables from tstattext which are always needed (for every type of plan).
     *
     * @param array $text tstattextaula
     * @return string[]
     */
    private function get_tstattext_content($text) {
        return array(
            $text[0]['Datumlang'],
            $text[0]['Druckdatum'],
            $text[0]['Version'],
            $text[0]['BitteBeachten'],
            $this->get_headings($text)
        );
    }

    /**
     * Return table headings found in tstattext.
     *
     * @param array $text tstattextaula
     * @return string[] headings
     */
    private function get_headings($text) {
        $headings = array();
        foreach (range(1, 7) as $num) {
            array_push($headings, $text[0]['H'.$num]);
        }
        return $headings;
    }

    /**
     * Return substitutions for the table from tdyntext. Each substitution represents one row in the table.
     *
     * @param array $text tdyntextaula
     * @return array
     */
    private function get_rows($text) {
        $substitutions = array();
        foreach ($text as $entry) {
            $row = array();
            foreach (range(1, 7) as $num) {
                array_push($row, $entry['F'.$num]);
            }
            array_push($substitutions, $row);
        }
        return $substitutions;
    }

    /**
     * Get the additional info from tstattext.
     *
     * @param array $text tstattext
     * @return array additional infos
     */
    private function get_additional_info($text) {
        $info = array();
        if ($text[0]["AbwKlassen"] !== "") {
            $info[] = $this->make_additional_entry(get_string('absentclasses', 'block_subplan'), $text[0]["AbwKlassen"]);
        }
        if ($text[0]["AbwKurse"] !== "") {
            $info[] = $this->make_additional_entry(get_string('absentcourses', 'block_subplan'), $text[0]["AbwKurse"]);
        }
        if ($text[0]["AbwLehrer"] !== "") {
            $info[] = $this->make_additional_entry(get_string('absentteachers', 'block_subplan'), $text[0]["AbwLehrer"]);
        }
        if ($text[0]["FehlRäume"] !== "") {
            $info[] = $this->make_additional_entry(get_string('missingrooms', 'block_subplan'), $text[0]["FehlRäume"]);
        }
        return $info;
    }

}
