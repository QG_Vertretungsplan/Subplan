<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Abstract base class for parsers.
 *
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_subplan\parsers;

/**
 * Abstract base class for parsers.
 */
abstract class parser_base {
    /**
     * Construct parser.
     *
     * @param str $input fetched from web.
     */
    abstract public function __construct($input);
    /**
     * Get data for all days.
     *
     * @return array array of two \block_subplan\output\day_data[], the first is for student, the latter for teachers.
     */
    abstract public function get_days();

    /**
     * Make an entry for the 'additionalinfo' array.
     *
     * @param string $name name to display
     * @param string $content content to display
     * @return string[] array that contains the name and the content
     */
    protected function make_additional_entry($name, $content) {
        return array("name" => $name, "content" => $content);
    }
}
