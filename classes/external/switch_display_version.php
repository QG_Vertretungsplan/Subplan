<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service for switching between student and teacher display version.
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_subplan\external;

use external_function_parameters;
use external_single_structure;
use external_value;

/**
 * Web service for switching between student and teacher display version.
 */
class switch_display_version extends \external_api {

    /**
     * Return description of parameters.
     *
     * @return external_function_parameters
     */
    public static function execute_parameters() {
        return new external_function_parameters([]);
    }

    /**
     * Return description of return values.
     *
     * @return void
     */
    public static function execute_returns() {
        return new external_single_structure(['result' => new external_value(PARAM_BOOL, 'Display teacher version')]);
    }

    /**
     * Switch between student and teacher display version.
     *
     * @return array Contains true if the selected version is the teacher version, else false.
     */
    public static function execute() {
        if (get_config("block_subplan", "enableteacherversion")) {
            $teacherpref = (bool) get_user_preferences("block_subplan_teacher", false);
            set_user_preference("block_subplan_teacher", !$teacherpref);
        } else {
            set_user_preference("block_subplan_teacher", false);
        }
        return ['result' => !$teacherpref];
    }
}
