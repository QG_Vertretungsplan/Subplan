<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Mobile output for subplan.
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_subplan\output;

/**
 * Output for mobile app.
 */
class mobile {

    /**
     * Function called by moodle to get content for the block
     * @return array content of the mobile block
     */
    public static function view_mobile() {
        global $PAGE, $CFG;
        require($CFG->dirroot . '/blocks/subplan/data.php');
        $renderable = get_plan_renderable();
        $renderable->set_mobile_version(true);
        $output = $PAGE->get_renderer('block_subplan');
        $html = $output->render($renderable);
        $js = 'setTimeout(function(){' . file_get_contents($CFG->dirroot . '/blocks/subplan/collapse.js') . '});';

        return [
            'templates' => [
                [
                    'id' => 'main',
                    'html' => $html,
                ],
            ],
            'javascript' => $js,
        ];
    }
}
