<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for all subplan elements.
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_subplan\output;

/**
 * Renderer for all subplan elements.
 */
class renderer extends \plugin_renderer_base {

    /**
     * Renders \block_subplan\substitution_plan_renderable
     *
     * @param \block_subplan\output\substitution_plan_renderable $page
     * @return string rendered html
     */
    public function render_substitution_plan($page) {
        $data = $page->export_for_template($this);
        // Use different templates for web/mobile version.
        if ($page->get_mobile_version()) {
            return parent::render_from_template('block_subplan/mobile', $data);
        } else {
            return parent::render_from_template('block_subplan/web', $data);
        }
    }
}
