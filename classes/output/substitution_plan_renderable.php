<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderable for a whole plan, for both web and mobile.
 *
 * @package   block_subplan
 * @copyright 2022 Quenstedt-Gymnasium Mössingen Vertretungsplanentwicklerteam
 * @author    Marcel, Richard, Michael, Frank Schiebel <frank@linuxmuster.net>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_subplan\output;

use stdClass;

/**
 * Renderable for a whole plan, for both web and mobile.
 */
class substitution_plan_renderable implements \renderable, \templatable {

    /**
     * If the data is valid.
     *
     * @var boolean
     */
    private $isvalid = false;
    /**
     * If a mobile version should be displayed.
     *
     * @var boolean
     */
    private $mobileversion = false;
    /**
     * E-Mail adress of an admin.
     *
     * @var string
     */
    private $mail;
    /**
     * Date (UNIX timestamp) from when the data was fetched.
     *
     * @var int
     */
    private $fetchdate;
    /**
     * Maximum amount of time (in seconds) that can pass before a warning will be displayed.
     *
     * @var int
     */
    private $maxage;
    /**
     * day_data for students.
     *
     * @var \block_subplan\output\day_data[]
     */
    private $studentplan;
    /**
     * day_data for teachers.
     *
     * @var \block_subplan\output\day_data[]
     */
    private $teacherplan;
    /**
     * Whether to display the button for switching to the teacher version.
     *
     * @var boolean
     */
    private $enableteacherversion;

    /**
     * Creates a renderable object from given day data.
     *
     * @param array $variants array of two \block_subplan\day_data[], the first is for student, the latter for teachers.
     */
    public function __construct($variants) {
        if (is_null($variants)) {
            return;
        }
        $this->isvalid = true;

        // For each day, save whether it is the last one, if it has any substitutions and generate a random id.
        // Also make replacements in the table in all columns specified in the settings.
        // Do all this for both the teacher version as well as the student version.
        $replacements = $this->get_replacements_from_settings();

        foreach ($variants as $key => $days) {
            // Load the columns in which replacements should be made (different for teachers/students).
            if ($key == 0) {
                $toreplace = $this->format_sequence(get_config('block_subplan', 'toreplacestudents'));
            } else {
                $toreplace = $this->format_sequence(get_config('block_subplan', 'toreplaceteachers'));
            }

            $daycontainers = array();
            for ($i = 0; $i < count($days); $i++) {
                $day = $days[$i];
                $this->make_replacements_in_day($day, $replacements, $toreplace);
                $dayid = \html_writer::random_id('block-subplan-');
                $hassubstitutions = $day->rows != null && $day->rows !== array();
                $daycontainer = (array) $day;
                $daycontainer["lastday"] = ($i === count($days) - 1);
                $daycontainer["hassubstitutions"] = $hassubstitutions;
                $daycontainer["dayid"] = $dayid;
                array_push($daycontainers, $daycontainer);
            }
            if ($key == 0) {
                $this->studentplan = $daycontainers;
            } else {
                $this->teacherplan = $daycontainers;
            }
        }

        $this->mail = get_config("block_subplan", "adminmail");
        $this->maxage = get_config("block_subplan", "tooOld");
        $this->fetchdate = time();
        $this->enableteacherversion = get_config("block_subplan", "enableteacherversion");
    }

    /**
     * Export a context that can be used to render a template.
     *
     * Add information which should be added at runtime (i.e. user-specific data) and shouldn't be cached in db.
     *
     * @param \renderer_base $output
     * @return stdClass
     */
    public function export_for_template(\renderer_base $output) {
        global $PAGE;
        $teacherversion = get_user_preferences("block_subplan_teacher", false);
        if (!$this->isvalid) {
            return new stdClass();
        }
        $data = new stdClass();
        if ($teacherversion) {
            $data->daycontainers = $this->teacherplan;
        } else {
            $data->daycontainers = $this->studentplan;
        }
        $data->mail = $this->mail;
        $data->fetchdate = $this->fetchdate;
        $data->isplantooold = $this->is_plan_too_old();
        $data->enableteacherversion = $this->enableteacherversion;

        // Data for the button that is displayed to switch versions (student/teacher).
        // Used to choose the correct string.
        $data->teacherversion = $teacherversion;
        // The web version requires a URL with correct params, the mobile version uses a web service.
        if (!$this->mobileversion) {
            // The user shall be able to switch to the other version, thus the negation.
            $data->switchversionurl = (new \moodle_url($PAGE->url, ['block_subplan_teacher' => !$teacherversion]))->out(true);
        }

        return $data;
    }

    /**
     * Set whether this renderable should be rendered as mobile or web version.
     *
     * @param boolean $mobileversion mobile version if true, else web version.
     * @return void
     */
    public function set_mobile_version($mobileversion) {
        $this->mobileversion = $mobileversion;
    }

    /**
     * Getter for 'mobileversion'.
     *
     * @return boolean
     */
    public function get_mobile_version() {
        return $this->mobileversion;
    }

    /**
     * Turns a PARAM_SEQUENCE from config into an array of individual numbers.
     *
     * @param string $sequence PARAM_SEQUENCE string
     * @return int[]
     */
    private function format_sequence($sequence) {
        return array_diff(explode(',', $sequence), array(''));
    }

    /**
     * Executes given replacements in the rows of the given day. Works on a reference.
     *
     * @param \block_subplan\day_data $day the day in whose rows replacements should be made.
     * @param array $replacements
     * @param int[] $toreplace column numbers of the columns where replacements should be made, starting at 1.
     * @return void
     */
    private function make_replacements_in_day(&$day, $replacements, $toreplace) {
        foreach ($day->rows as $rowindex => $row) {
            foreach ($toreplace as $num) {
                $replaced = $this->make_replacements($row[$num - 1], $replacements);
                $day->rows[$rowindex][$num - 1] = $replaced;
            }
        }
    }

    /**
     * Replaces substrings and adds css classes according to the settings
     * @param string $inputtext
     * @param array $replacements
     * @return string
     */
    private function make_replacements($inputtext, $replacements) {
        foreach ($replacements as $replacement) {
            list($needle, $replacementtext, $cssclass) = explode("|", $replacement);
            $needle = trim($needle);
            $replacementtext = trim($replacementtext);
            $cssclass = trim($cssclass);
            if (!isset($cssclass)) {
                $inputtext = str_replace($needle, $replacementtext, $inputtext);
            } else {
                $inputtext = str_replace($needle, $this->html_tag_text($replacementtext, "span", $cssclass), $inputtext);
            }
        }
        return $inputtext;
    }

    /**
     * Add html tags to string
     * @param string $text
     * @param string $htmltag HTML Tag to wrap around $text
     * @param string $cssclasses CSS Classes for HTML-Tag
     * @return string
     */
    private function html_tag_text($text, $htmltag, $cssclasses) {
        $html  = "<" . $htmltag . " class=\"" . $cssclasses . "\">" . $text . "</$htmltag>";
        return $html;
    }

    /**
     * Get the replacement settings and process and save them
     * @return array replacements
     */
    private function get_replacements_from_settings() {
        $replacements = get_config("block_subplan", "replacements");
        $lines = preg_split('/;/', $replacements);
        foreach ($lines as $line) {
            // Ignore comments (except escaped ones).
            $line = preg_replace('/(?<![&\\\\])#.*$/', '', $line);
            $line = str_replace('\\#', '#', $line);
            $line = trim($line);
            if ($line === '') {
                continue;
            }
            $line = preg_split('/\s+/', $line, 2);
            $line = array_pad($line, 2, '');
            $subst[$line[0]] = $line[1];
        }
        if (isset($subst)) {
            return $subst;
        }
    }


    /**
     * Returns whether the plan is so old that a warning should be displayed.
     *
     * @return boolean
     */
    private function is_plan_too_old() {
        return $this->fetchdate !== null && (time() - $this->fetchdate) > $this->maxage;
    }
}
